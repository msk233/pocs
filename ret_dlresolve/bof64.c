#include <unistd.h>
#include <stdio.h>
#include <string.h>

__asm__(
    "pop rdi\n"
    "pop rsi\n"
    "pop rdx\n"
    "ret"
);

void vuln()
{
    char buf[100];
    setbuf(stdin, buf);
    read(0, buf, 256);
}
int main()
{
    char buf[100] = "Welcome to XDCTF2015~!\n";

    setbuf(stdout, buf);
    write(1, buf, strlen(buf));
    vuln();
    return 0;
}