#include<stdio.h>
#include<stdlib.h>

char * padding[0x100];
void * bss;

int main(){
    void * pre = malloc(0x80);
    void * ptr = malloc(0x80);
    void * head = ptr - 0x10;
    bss = pre;
    printf("%p\n",bss);
    ((long long *)ptr)[-1] = 0x90;
    ((long long *)ptr)[-2] = 0x80;

    ((long long *)pre)[1] = 0x81;
    ((long long *)pre)[2] = 0x601080 - 0x18;
    ((long long *)pre)[3] = 0x601080 - 0x10;

    free(ptr);
    printf("%p\n",bss);
    return 0;
}
