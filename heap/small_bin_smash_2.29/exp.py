from PwnContext import *
context.log_level = 'debug'

ctx.binary = './example'
ctx.remote_libc = '/home/msk/glibc-all-in-one/libs/2.29-0ubuntu2_amd64/libc-2.29.so'
ctx.debug_remote_libc = True

ctx.start()
ctx.debug()
ctx.sendline('a')
ctx.interactive()