#include<stdio.h>
#include<stdlib.h>

int main(){
    unsigned long uv = 0x7fffdeadbeef;
    int*v = &uv;
    unsigned long long sum = 0;
    printf("low 32 bits transform into int is %d\n",*v);
    printf("but its true size is %u\n",*((unsigned*)v));
    sum = 3735928559+559038737;
    printf("sum is %llx",sum);
    return 0;
}