#include<stdio.h>
#include<string.h>
#include<dlfcn.h>

typedef int(*STRCMP)(const char*, const char*);

int strcmp(const char* s1, const char* s2){
    static void* handle = NULL;
    static STRCMP old = NULL;

    if(!handle){
        handle = dlopen("libc.so.6",RTLD_LAZY);
        old = (STRCMP)dlsym(handle,"strcmp");
    }
    printf("Hook it !");
    return old(s1,s2);
}