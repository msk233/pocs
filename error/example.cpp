#include<iostream>
using namespace std;

int callee(int c){
    if(c==3){
        throw "c == 3";
    }
    return c+1;
}

int caller(int a, int b){
    int c = a + b;
    try{
        callee(c);
    }catch (const char* msg){
        cerr<<msg<<endl;
    }
    return 0;
}


int main(){
    int a = 1;
    int b = 2;
    return caller(a, b);
}