#include<stdio.h>
#include<stdlib.h>

int main(){
    int i = 0;
    for(i;i<3;i++){
        int pid;
        if((pid = fork())==0){
            printf("This is child %d, pid = %d\n",i,pid);
        }
        else{
            printf("This is father %d, pid = %d\n",i,pid);
            wait();
        }
    }
}