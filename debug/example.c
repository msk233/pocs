#include<stdio.h>
#include<stdlib.h>

typedef struct{
    int key;
    int value;
}KV;

int main(){
    KV *kv1 = malloc(sizeof(KV));
    printf("kv1 = %p\n",kv1);
    kv1->key = 123456;
    kv1->value = 654321;
    printf("kv1->key = %d\n",kv1->key);
    printf("kv1->value = %d\n",kv1->value);
    return 0;
}