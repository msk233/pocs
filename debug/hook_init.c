// gcc -g -fPIC -shared hooc_init.c -o hook_init.so -ldl
#include <stdio.h>
#include <dlfcn.h>

void my_init(void) __attribute__((constructor));

void my_init()
{
    printf("image base addr: %p\n", *(char **)dlopen(NULL, RTLD_LAZY));
    printf("libc base addr: %p\n", *(char **)dlopen("libc.so.6", RTLD_LAZY));
}